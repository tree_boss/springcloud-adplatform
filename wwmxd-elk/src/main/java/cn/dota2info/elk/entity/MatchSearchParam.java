package cn.dota2info.elk.entity;

import lombok.Builder;
import lombok.Data;

@Data
public class MatchSearchParam extends BaseSearchParam {
    private Integer prefixLength;
    private Integer maxExpansions;
}
